﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RadioDJ;
using RadioDJ.Caller;
using RadioDJ.Database;
using RadioDJ.Reports;
using RadioDJ.Speech2Text;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;

var processes = Process.GetProcessesByName("RadioDJ");
if(processes.Length > 1)
{
    MessageBox.Show("Процесс уже запущен", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
    return;
}

var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false)
                    .Build();

await new HostBuilder()
    .ConfigureServices((hostContext, services) =>
    {
        services.AddHostedService<RadioDjService>();

        services.AddSingleton<IRecognizable, Golos>(_ => new Golos(configuration["golos_url"], LoggerFactory.Create(builder => builder.AddConsole()).CreateLogger<Golos>()));
        services.AddSingleton<IDbRadioQueries, Digispot2Quieries>(_ => new Digispot2Quieries(new SqlConnection(configuration["connection_string"])));
        services.AddSingleton<ISoftPhone, HuaweiE171Caller>();
        services.AddSingleton<IReportFormer, HtmlReportFormer>();
        services.AddLogging();
    })
    .ConfigureLogging((hostContext, configLogging) =>
    {
        configLogging.AddConsole();
        configLogging.AddDebug();
    })
    .RunConsoleAsync();





