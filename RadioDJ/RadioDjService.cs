﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NAudio.Wave;
using RadioDJ.Caller;
using RadioDJ.Database;
using RadioDJ.Reports;
using RadioDJ.Searchers;
using RadioDJ.Speech2Text;

namespace RadioDJ
{
    public class RadioDjService : IHostedService
    {
        private readonly IDbRadioQueries _db;
        private readonly ISoftPhone _softPhone;
        private readonly IRecognizable _speechToText;
        private readonly ILogger<RadioDjService> _logger;
        private readonly IReportFormer _reportFormer;
        public RadioDjService(IDbRadioQueries db, ISoftPhone softPhone, IRecognizable speechToText, ILogger<RadioDjService> logger, IReportFormer reportFormer)
        {
            _db = db;
            _speechToText = speechToText;
            _softPhone = softPhone;
            _logger = logger;
            _reportFormer = reportFormer;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Delay(500);
            _softPhone.OnCallRecorded += OnCallRecorded;
            _softPhone.WaitCall();
            return Task.CompletedTask;
        }
        private void OnCallRecorded(string phoneNumber, byte[] audio)
        {
            _logger.LogInformation("Получена запись разговора. Номер: " + phoneNumber);
            var path = $"./call_{DateTime.Now:yyyy_MM_dd_HH_mm_ss}/";
            _logger.LogInformation("Директория: " + path);
            Directory.CreateDirectory(path);
            SaveWaveFile(audio, path + "record.wav");
            string text = "";
            List<string> songs;
            try
            {
                text = _speechToText.RecognizeSpeech(File.ReadAllBytes(path + "record.wav"));
                songs = _db.GetSongs().ToList();
            }
            catch(Exception e)
            {
                _logger.LogCritical(e.Message);
                _softPhone.Stop();
                Environment.Exit(-1);
            }
            var searcher = new LevensteinBasedTextSearcher();
            var songKeywords = new string[] { "песню", "подарок" };
            var authorKeywords = new string[] { "песню", "подарок", "исполнении" };
            var index = searcher.FindLastIndexOfKeyword(text, authorKeywords);
            string? report;
            if (index == -1)
            {
                _logger.LogInformation("Поиск отменен");
                report = _reportFormer.FormReport(text);
            }
            else
            {
                var authorFirst = FindAuthorFirst(text, authorKeywords, songKeywords);
                if (authorFirst != null)
                {
                    report = _reportFormer.FormReport(text, authorFirst.Song, authorFirst.Author, authorFirst.Filename);
                }
                else
                {
                    var songFirst = FindSongFirst(text, authorKeywords, songKeywords);
                    if (songFirst != null)
                    {
                        report = _reportFormer.FormReport(text, songFirst.Song, songFirst.Author, songFirst.Filename);
                    }
                    else
                    {
                        songs = _db.GetSongs().ToList();
                        var song = searcher.FindWord(text, songs, songKeywords);
                        report = _reportFormer.FormReport(text, _db.GetAuthorsByTrack(song.inList));
                    }
                }
            }
            using (var writer = new StreamWriter(File.OpenWrite(path + "report.html")))
            {
                writer.Write(report);
            }
            _logger.LogInformation("Отчет сформирован");
        }
        private SongInfo FindAuthorFirst(string text, string[] authorKeywords, string[] songsKeywords)
        {
            var authors = _db.GetAuthors();
            var songs = _db.GetSongs();
            var searcher = new LevensteinBasedTextSearcher();
            var foundAuthor = searcher.FindWord(text, authors, authorKeywords);
            if (foundAuthor.inList == "")
            {
                return null;
            }
            var textWithoutAuthor = text.Replace(foundAuthor.inText, "");
            var foundSong = searcher.FindWord(textWithoutAuthor, songs, songsKeywords);
            return _db.GetTrack(foundAuthor.inList, foundSong.inList);
        }
        private SongInfo FindSongFirst(string text, string[] authorKeywords, string[] songsKeywords)
        {
            var authors = _db.GetAuthors();
            var songs = _db.GetSongs();
            var searcher = new LevensteinBasedTextSearcher();
            var foundSong = searcher.FindWord(text, songs, songsKeywords);
            if(foundSong.inList == "")
            {
                return null;
            }
            var textWithoutSong = text.Replace(foundSong.inText, "");
            var foundAuthor = searcher.FindWord(textWithoutSong, authors, authorKeywords);
            return _db.GetTrack(foundAuthor.inList, foundSong.inList);
        }
        private void SaveWaveFile(byte[] audio, string path)
        {
            using (var writer = new WaveFileWriter(path, new WaveFormat(8000, 16, 1)))
            {
                writer.WriteData(audio, 0, audio.Length);
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _softPhone.Stop();
            return Task.CompletedTask;
        }
    }
}
