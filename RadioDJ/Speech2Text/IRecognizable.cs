﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Speech2Text
{
    public interface IRecognizable
    {
        public string RecognizeSpeech(byte[] wavAudio);
    }
}
