﻿using Microsoft.Extensions.Logging;
using System.Net;

namespace RadioDJ.Speech2Text
{
    public class Golos : IRecognizable, IRecognizableAsync
    {
        private readonly string _serverUrl;
        private readonly ILogger<Golos> _logger;
        public Golos(string serverUrl, ILogger<Golos> logger)
        {
            _serverUrl = serverUrl;
            _logger = logger;
        }
        public string RecognizeSpeech(byte[] audio)
        {
            var result = "";
            _logger.LogInformation($"Получен аудиофайл длиной {audio.Length} байт");
            _logger.LogInformation("Подключение к серверу " + _serverUrl);
            var httpRequest = WebRequest.Create(_serverUrl) as HttpWebRequest;
            _logger.LogInformation("Формирование запроса");
            httpRequest.Method = "POST";
            httpRequest.ContentType = "application/json";
            string data = Convert.ToBase64String(audio);
            using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
            {
                _logger.LogInformation("Отправка запроса");
                string json = "{\"file\":\"" + data + "\" }";
                streamWriter.Write(json);
            }
            _logger.LogInformation("Получение ответа");
            var httpResponse = httpRequest.GetResponse() as HttpWebResponse;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            _logger.LogInformation($"Ответ получен. Длина {result.Length} байт");
            return result.Trim('[', ']', '"');
        }
        public async Task<string> RecognizeSpeechAsync(byte[] audio)
        {
            var result = "";
            _logger.LogInformation($"Получен аудиофайл длиной {audio.Length} байт");
            _logger.LogInformation("Подключение к серверу " + _serverUrl);
            var httpRequest = WebRequest.Create(_serverUrl) as HttpWebRequest;
            _logger.LogInformation("Формирование запроса");
            httpRequest.Method = "POST";
            httpRequest.ContentType = "application/json";
            string data = Convert.ToBase64String(audio);
            using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
            {
                _logger.LogInformation("Отправка запроса");
                string json = "{\"file\":\"" + data + "\" }";
                await streamWriter.WriteAsync(json);
            }
            _logger.LogInformation("Получение ответа");
            var httpResponse = await httpRequest.GetResponseAsync() as HttpWebResponse;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                 result = await streamReader.ReadToEndAsync();
            }
            _logger.LogInformation($"Ответ получен. Длина {result.Length} байт");
            return result.Trim('[', ']', '"');
        }
    }
}
