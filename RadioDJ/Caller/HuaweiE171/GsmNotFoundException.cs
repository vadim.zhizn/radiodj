﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Infrastructure
{
    public class GsmNotFoundException : Exception
    {
        public GsmNotFoundException()
        {

        }
        public GsmNotFoundException(string message) :base(message)
        {

        }
        public GsmNotFoundException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
