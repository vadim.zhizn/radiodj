﻿using Microsoft.Extensions.Logging;
using RadioDJ.Infrastructure;
using System.IO.Ports;
using System.Management;

namespace RadioDJ.Caller
{
    public class HuaweiE171Caller : ISoftPhone
    {
        public event ISoftPhone.CallRecorded OnCallRecorded;
        private readonly SerialPort _commandPort;
        private readonly SerialPort _voicePort;
        private readonly ILogger<HuaweiE171Caller> _logger;
        private bool isListenerStopped;
        private int status = 1;
        public HuaweiE171Caller(ILogger<HuaweiE171Caller> logger)
        { 
            _logger = logger;
            var found = FindPorts();
            _commandPort = new SerialPort(found.commandPortName);
            _voicePort = new SerialPort(found.voicePortName);
        }
       
        public async Task WaitCall()
        {
            OpenPort(_commandPort);
            _commandPort.Write("AT+CLIP=1;\r\n");
            while (true)
            {
                string portAnswer = "";
                while (!portAnswer.Contains("RING") && !isListenerStopped)
                {
                    await Task.Delay(100);
                    portAnswer = _commandPort.ReadLine();
                    _logger.LogInformation(portAnswer);
                }
                if(isListenerStopped)
                {
                    status = 0;
                    return;
                }
                while (!portAnswer.Contains("CLIP"))
                {
                    portAnswer = _commandPort.ReadLine();
                    _logger.LogInformation(portAnswer);
                }
                var phoneNumber = portAnswer.Substring(8, 12);
                _commandPort.Write("ATA;\r\n");
                var audio = RecordCall();
                OnCallRecorded?.Invoke(phoneNumber, audio);
            }
        }
        public async Task Stop()
        {
            isListenerStopped = true;
            while(status != 0) { }
            _commandPort.Close();
            _voicePort.Close();

        }
        private byte[] RecordCall()
        {
            _logger.LogInformation("Начало записи звонка");
            _commandPort.Write("AT^DDSETEX=2;\r\n");
            OpenPort(_voicePort);
            var bytes = new List<byte>(200000);
            var stream = _voicePort.BaseStream;
            var tmp = new byte[8000];
            while (true)
            {
                var portAnswer = _commandPort.ReadLine();
                _logger.LogInformation(portAnswer);
                stream.Read(tmp, 0, tmp.Length);
                bytes.AddRange(tmp);
                Thread.Sleep(20);

                if (portAnswer.Contains("CEND:1"))
                {
                    if (bytes.Count > 0)
                    {
                        Thread.Sleep(100);
                        tmp = new byte[64000];
                        stream.Read(tmp, 0, tmp.Length);
                        bytes.AddRange(tmp);
                    }
                    break;
                }
            }
            _logger.LogInformation("Запись завершена");
            _voicePort.Close();
            stream.Close();
            return bytes.ToArray();
        }

        private void OpenPort(SerialPort port)
        {
            if(port.IsOpen)
            {
                port.Close();
            }
            port.Open();
        }
        private (string commandPortName, string voicePortName) FindPorts()
        {
            string query = "SELECT * FROM Win32_POTSModem";
            _logger.LogInformation("Поиск порта, занимаемого модемом");
            ManagementObjectSearcher searcher = new(query);
            string modemPort = "";
            foreach (var device in searcher.Get())
            {
                modemPort = device["Name"].ToString() + "(" + device["AttachedTo"].ToString() + ")";
            }
            if (string.IsNullOrEmpty(modemPort))
            {
                _logger.LogCritical(" Аварийное завершение");
                Environment.Exit(-1);
            }
            _logger.LogInformation("Найден порт: " + modemPort);
            _logger.LogInformation("Поиск COM портов");
            modemPort = modemPort.Substring(0, modemPort.LastIndexOf("-"));
            searcher = new ManagementObjectSearcher("root\\cimv2", "SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'");
            string voicePortName = "", commandPortName = "";
            foreach (var obj in searcher.Get())
            {
                string temp = obj["Caption"].ToString();
                if (temp.Contains("Application") && temp.Contains(modemPort))
                {
                    voicePortName = temp;
                }
                if (temp.Contains("UI") && temp.Contains(modemPort))
                {
                    commandPortName = temp;
                }
            }
            voicePortName = voicePortName.Substring(voicePortName.IndexOf("COM"), voicePortName.IndexOf(")") - voicePortName.IndexOf("(") - 1);
            commandPortName = commandPortName.Substring(commandPortName.IndexOf("COM"), commandPortName.IndexOf(")") - commandPortName.IndexOf("(") - 1);
            _logger.LogInformation($"Голосовой порт занимает {voicePortName}");
            _logger.LogInformation($"Командный порт занимает {commandPortName}");
            return (commandPortName, voicePortName);
        }

    }
}
