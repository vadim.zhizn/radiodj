﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Caller
{
    public interface ISoftPhone
    {
        public delegate void CallRecorded(string phoneNumber, byte[] audio);
        public event CallRecorded OnCallRecorded;
        public Task WaitCall();
        public Task Stop();
    }
}
