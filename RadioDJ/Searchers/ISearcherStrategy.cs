﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Searchers
{
    public interface ISearcherStrategy
    {
        (string inText, string inList) FindWord(string text, IEnumerable<string> words, int maxDifference);
        (string inText, string inList) FindWord(string text, IEnumerable<string> words, IEnumerable<string> keywords, int maxDifference);
    }
}
