﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Searchers
{
    public class LevensteinBasedTextSearcher : ISearcherStrategy
    {
        public (string inText, string inList) FindWord(string text, IEnumerable<string> words, IEnumerable<string> keywords, int maxDifference = 4)
        {
            var index = FindLastIndexOfKeyword(text, keywords);
            return index == -1 ? (string.Empty, string.Empty) : FindWord(text.Substring(index), words);
        }
        public (string inText, string inList) FindWord(string text, IEnumerable<string> words, int maxDifference = 4)
        {
            var min = int.MaxValue;
            var found = string.Empty;
            var inList = string.Empty;
            var textWords = text.Split();
            foreach(var word in words)
            {
                var normString = word.Replace(",", "").Replace(" ", "").ToLower();
                var countWords = word.Split().Count();
                var end = countWords + 2 < textWords.Count() ? countWords + 2 : textWords.Count();

                for (int i = 1; i < end; i++)
                {
                    var comparableString = "";
                    var stringFromText = "";
                    for (int j = 1; j <= i; j++)
                    {
                        comparableString += textWords[j];
                        stringFromText += textWords[j] + " ";
                    }
                    if (comparableString.Length <= 3 && comparableString != "")
                    {
                        continue;
                    }
                    int res = Levenstein.Distance(comparableString, normString);
                    if (res <= min)
                    {
                        min = res;
                        found = stringFromText;
                        inList = word;
                    }
                }
            }
            return min < maxDifference && maxDifference >= 0 ? (found, inList) : (string.Empty, string.Empty);
        }
        public int FindLastIndexOfKeyword(string text, IEnumerable<string> keywords, int maxDifference = 3)
        {
            int[] indexes = new int[keywords.Count()];
            for (int i = 0; i < keywords.Count(); i++)
            {
                indexes[i] = Levenstein.IndexOfWord(text, keywords.ElementAt(i));
            }
            var index = indexes.Max();
            return index;
        }
    }
}
