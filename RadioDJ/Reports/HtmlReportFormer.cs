﻿using RadioDJ.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Reports
{
    public class HtmlReportFormer : IReportFormer
    {
        private string _startString = "<!DOCTYPE html>" +
                                              "<html>" +
                                              "<head>" +
                                              "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">" +
                                              "<title>Отчёт</title>" +
                                              "</head>" +
                                              "<body>";
        public string FormReport(string text, string song, string author, string filename)
        {
            return _startString +
                      "<table class=\"table table-bordered p-1 m-1\">" +
                      "<tr><th>Распознанный текст</th><th>Найденная песня</th><th>Найденный автор</th><th>Путь</th></tr>"+
                        $"<tr><td>{text}</td><td>{song}</td><td>{author}</td><td>{filename}</td></tr>"+
                      "</table>"+
                      "</body>"+
                      "</html>";

        }
        public string FormReport(string text, IEnumerable<SongInfo> songsAndFilenames)
        {
            var result = "<table class=\"table table-bordered p-1 m-1\">" +
                         "<tr><th>Распознанный текст</th><th>Найденная песня</th><th>Автор</th><th>Путь до файла</th></tr>" +
                         $"<tr><td rowspan=\"{songsAndFilenames.Count()}\">{text}</td>";
            for(int i = 0; i < songsAndFilenames.Count(); i++)
            {
                if(i != 0)
                {
                    result += "<tr>";
                }
                result += $"<td>{songsAndFilenames.ElementAt(i).Song}</td><td>{songsAndFilenames.ElementAt(i).Author}</td><td>{songsAndFilenames.ElementAt(i).Filename}</td></tr>";
            }
            result += "</table></body></html>";
            return _startString + result;
        }
        public string FormReport(string text)
        {
            return _startString +
                "<table class=\"table table-bordered p-1 m-1\">" +
                      "<tr><th>Распознанный текст</th></tr>" +
                        $"<tr><td>{text}</td></tr>" +
                      "</table>" +
                      "</body>" +
                      "</html>";
        }

    }
}
