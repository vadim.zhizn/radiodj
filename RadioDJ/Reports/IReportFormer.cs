﻿using RadioDJ.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Reports
{
    public interface IReportFormer
    {
        string FormReport(string text, string song, string author, string filename);
        string FormReport(string text, IEnumerable<SongInfo> songsAndFilenames);
        string FormReport(string text);
    }
}
