﻿namespace RadioDJ.Database
{
    public interface IDbRadioQueries
    {
        public IEnumerable<string> GetSongs();
        public IEnumerable<string> GetAuthors();
        public SongInfo GetTrack(string author, string song);
        public IEnumerable<SongInfo> GetAuthorsByTrack(string song);
    }
}
