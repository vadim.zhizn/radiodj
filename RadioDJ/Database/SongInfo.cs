﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Database
{
    public class SongInfo
    {
        public string Author { get; set; }
        public string Song { get; set; }
        public string Filename { get; set; }
    }
}
