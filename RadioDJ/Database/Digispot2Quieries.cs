﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioDJ.Database
{
    public class Digispot2Quieries : IDbRadioQueries
    {
        private readonly SqlConnection _connection;
        public Digispot2Quieries(SqlConnection connection) => _connection = connection;
        public IEnumerable<string> GetAuthors()
        {
            List<string> result = new List<string>();
            _connection.Open();
            SqlCommand command = new SqlCommand();
            command.CommandText = @"SELECT DISTINCT dbo.ATTRIB_VALS.Name FROM PH_TO_ATTRVALS
                                        JOIN PH ON PH_TO_ATTRVALS.ph_id = PH.id
                                        JOIN ATTRIB_VALS ON PH_TO_ATTRVALS.attrval_id = ATTRIB_VALS.id
										WHERE dbo.ATTRIB_VALS.Name NOT IN('Вести Fm Оформление', 'Вести Fm Оформление\Руслан', 'Волгоградская Фонотека\Шумы', 'Реклама', 'Редакторы\Шевченко Инесса',
										'ShevcIY', 'Редакторы\Шевченко Инесса\готовые сюжеты', 'Звукорежиссеры\Кандалова Мария', 'Реклама\Архив реклама',
										'The Author', 'Кулишенко Елена', 'Кулишенко Елена', 'TopilNV', 'SnizhNN', 'RiaboAV', 'Рябова Анна', 'GazhMV',
										'KandaMN', 'airuser02', 'PalguIE', 'KikinSA', 'PalguIE', 'CimbaOD', 'RadchVV', 'Звукорежиссеры\Снижко Наталья', 'RadchVV', 'VoronNS', 'DjumaDM',
										'FurmanTV', 'PolykOG', 'Маяк Оформление', 'Волгоградская Фонотека\Инструментальная', 'Фурманова Татьяна', 'Гажук Марина', 'Урьев Глеб',
										'Маяк Оформление\Новые рекламные шапки', 'Реклама Радио Маяк', 'admin', 'Волгоград 24 Фм', 'DjgarVU', 'VLG_24_JINGLE\САМЫЙ ЛУЧШИЙ ЧАС',
										'Волгоград 24', 'Радио России\Архив', 'KulisEV', 'UsachSN',	'Rolik Fm', 'VLG_24\OLDIES-Zapad', 'VLG_24\CLOCK', 'VLG_24\GOLD-Zapad', 'VLG_24 NEW\!!! САМЫЕ-САМЫЕ\АРХИВ', 'Заявки в презент',
										'Кикина Света', 'Пальгунов Эдуард', 'VOLGOGRAD24\06_КЛАУС', 'ChernAS',	'VLG_24_JINGLE\!!! ПРОМО В ЭФИРЕ\АРХИВ', 'VLG_24 NEW\!!! САМЫЕ-САМЫЕ', 'Валерия Лушникова',
										'Манкевич Анастасия', 'Артюшина Татьяна Ивановна', 'Новиков Дмитрий', '>>>>>>>ПОГОДАС<<<<<<<', 'Арсеньев Сергей', ':::УТРЕННИЙ КОНКУРС_КОДА:::', 'Ковалёв Павел',
										'VLG_24_JINGLE')
                                        AND dbo.ATTRIB_VALS.Name NOT LIKE 'Радио России\Эфир%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'Радио России\Для Эфира%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'Микрофонная папка\Радио России\%'  
										AND dbo.ATTRIB_VALS.Name NOT LIKE 'Микрофонная папка\Вести FM%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'News Room\Гажук Марина\Верность Отечеству%'
										AND dbo.ATTRIB_VALS.Name NOT LIKE 'Отбивки%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'VOLGOGRAD24%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'News Room%' 
										AND dbo.ATTRIB_VALS.Name NOT LIKE 'VLG_24 NEW%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'Вести ФМ%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'airuser%'
										AND dbo.ATTRIB_VALS.Name NOT LIKE '%MOROZILKA%' AND dbo.ATTRIB_VALS.Name NOT LIKE '%САМЫЕ_САМЫЕ%' AND dbo.ATTRIB_VALS.Name NOT LIKE '%>>>%' ";

            command.Connection = _connection;
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(0).ToString());
                }
            }
            _connection.Close();
            return result;
        }

        public IEnumerable<string> GetSongs()
        {
            List<string> result = new List<string>();
            _connection.Open();
            SqlCommand command = new SqlCommand();

            command.CommandText = @"SELECT DISTINCT dbo.PH.Name FROM PH_TO_ATTRVALS
                                        JOIN PH ON PH_TO_ATTRVALS.ph_id = PH.id
                                        JOIN ATTRIB_VALS ON PH_TO_ATTRVALS.attrval_id = ATTRIB_VALS.id
										WHERE dbo.ATTRIB_VALS.Name NOT IN('Вести Fm Оформление', 'Вести Fm Оформление\Руслан', 'Волгоградская Фонотека\Шумы', 'Реклама', 'Редакторы\Шевченко Инесса',
										'ShevcIY', 'Редакторы\Шевченко Инесса\готовые сюжеты', 'Звукорежиссеры\Кандалова Мария', 'Реклама\Архив реклама',
										'The Author', 'Кулишенко Елена', 'Кулишенко Елена', 'TopilNV', 'SnizhNN', 'RiaboAV', 'Рябова Анна', 'GazhMV',
										'KandaMN', 'airuser02', 'PalguIE', 'KikinSA', 'PalguIE', 'CimbaOD', 'RadchVV', 'Звукорежиссеры\Снижко Наталья', 'RadchVV', 'VoronNS', 'DjumaDM',
										'FurmanTV', 'PolykOG', 'Маяк Оформление', 'Волгоградская Фонотека\Инструментальная', 'Фурманова Татьяна', 'Гажук Марина', 'Урьев Глеб',
										'Маяк Оформление\Новые рекламные шапки', 'Реклама Радио Маяк', 'admin', 'Волгоград 24 Фм', 'DjgarVU', 'VLG_24_JINGLE\САМЫЙ ЛУЧШИЙ ЧАС',
										'Волгоград 24', 'Радио России\Архив', 'KulisEV', 'UsachSN',	'Rolik Fm', 'VLG_24\OLDIES-Zapad', 'VLG_24\CLOCK', 'VLG_24\GOLD-Zapad', 'VLG_24 NEW\!!! САМЫЕ-САМЫЕ\АРХИВ', 'Заявки в презент',
										'Кикина Света', 'Пальгунов Эдуард', 'VOLGOGRAD24\06_КЛАУС', 'ChernAS',	'VLG_24_JINGLE\!!! ПРОМО В ЭФИРЕ\АРХИВ', 'VLG_24 NEW\!!! САМЫЕ-САМЫЕ', 'Валерия Лушникова',
										'Манкевич Анастасия', 'Артюшина Татьяна Ивановна', 'Новиков Дмитрий', '>>>>>>>ПОГОДАС<<<<<<<', 'Арсеньев Сергей', ':::УТРЕННИЙ КОНКУРС_КОДА:::', 'Ковалёв Павел',
										'VLG_24_JINGLE')
                                        AND dbo.ATTRIB_VALS.Name NOT LIKE 'Радио России\Эфир%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'Радио России\Для Эфира%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'Микрофонная папка\Радио России\%'  
										AND dbo.ATTRIB_VALS.Name NOT LIKE 'Микрофонная папка\Вести FM%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'News Room\Гажук Марина\Верность Отечеству%'
										AND dbo.ATTRIB_VALS.Name NOT LIKE 'Отбивки%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'VOLGOGRAD24%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'News Room%' 
										AND dbo.ATTRIB_VALS.Name NOT LIKE 'VLG_24 NEW%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'Вести ФМ%' AND dbo.ATTRIB_VALS.Name NOT LIKE 'airuser%'
										AND dbo.ATTRIB_VALS.Name NOT LIKE '%MOROZILKA%' AND dbo.ATTRIB_VALS.Name NOT LIKE '%САМЫЕ_САМЫЕ%' AND dbo.ATTRIB_VALS.Name NOT LIKE '%>>>%' ";
										
            command.Connection = _connection;
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(0).ToString());
                }
            }
            _connection.Close();
            return result;
        }

        public SongInfo GetTrack(string author, string song)
        {
            SongInfo result = null;
            _connection.Open();
            SqlCommand command = new SqlCommand();
            command.CommandText = $@"SELECT  PH.FileName FROM PH_TO_ATTRVALS
                                        JOIN PH ON PH_TO_ATTRVALS.ph_id = PH.id
                                        JOIN ATTRIB_VALS ON PH_TO_ATTRVALS.attrval_id = ATTRIB_VALS.id 
                                        WHERE dbo.ATTRIB_VALS.Name LIKE '{author}' AND dbo.PH.Name LIKE '{song}'";

            command.Connection = _connection;
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                result = new SongInfo { Author = author, Song = song, Filename = reader.GetValue(0).ToString() };
            }
            _connection.Close();
            return result;
        }

        public IEnumerable<SongInfo> GetAuthorsByTrack(string song)
        {
            var result = new List<SongInfo>();
            _connection.Open();
            SqlCommand command = new SqlCommand();
            command.CommandText = $@"SELECT  dbo.ATTRIB_VALS.Name, PH.FileName FROM PH_TO_ATTRVALS
                                        JOIN PH ON PH_TO_ATTRVALS.ph_id = PH.id
                                        JOIN ATTRIB_VALS ON PH_TO_ATTRVALS.attrval_id = ATTRIB_VALS.id WHERE dbo.PH.Name LIKE '{song}'";

            command.Connection = _connection;
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var name = reader.GetValue(0).ToString();
                    var filename = reader.GetValue(1).ToString();
                    result.Add(new SongInfo { Author = name, Song = song, Filename = filename });
                }
            }
            _connection.Close();
            return result;
        }
        
    }
}
